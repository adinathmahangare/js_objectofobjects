//Q3 Find all users with masters Degree.

function getUserWithMastersDegree(userData) {

    if (typeof userData == 'object' && userData != null) {

        for (const user in userData) {
            const userInfo = userData[user];
            if (userInfo.qualification === "Masters") {
                console.log(user);
            }
        }
    }else {
        return console.log("Given data is not in correct format");
    }
}

module.exports = getUserWithMastersDegree;