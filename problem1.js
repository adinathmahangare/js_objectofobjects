//Q1 Find all users who are interested in playing video games.

function getUserPlayingVideoGame(userData) {
    if (typeof userData == 'object' && userData != null) {

        for (const user in userData) {
            const userInfo = userData[user];

            let videoGames = "Video Games";
            let videoGameslength = videoGames.length;

            if (Array.isArray(userInfo.interests)) {
                let interestString = userInfo.interests[0];  
            
                for (let pointer1 = 0; pointer1 < interestString.length; ++pointer1) {
                    let pointer2 = 0;
                    while (pointer2 < videoGameslength && interestString[pointer1 + pointer2] == videoGames[pointer2]) {
                        ++pointer2;
                    }
                    if (pointer2 == videoGameslength) {
                        console.log(user);
                    }
                }
            } else {
                return console.log("Given data is not in correct format");
            }
            
        }
    } else {
        return ;
    }
    
}

module.exports = getUserPlayingVideoGame;