//Q2 Find all users staying in Germany.

function getAllUsersInGermany(userData) {
    if (typeof userData == 'object' && userData != null) {
        for (const user in userData) {
            const userInfo = userData[user];
            if (userInfo.nationality === "Germany") {
                console.log(user);
            }
        }
    }else {
        return console.log("Given data is not in correct format");
    }
    
}

module.exports = getAllUsersInGermany;