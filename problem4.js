//Q4 Group users based on their Programming language mentioned in their designation.

function groupUsersProgramming(userData) {

    if (typeof userData == 'object' && userData != null) {
        const languageGroups = {};

        for (const user in userData){
            const userInfo = userData[user];
            const parts = userInfo.desgination.split(" ");

            let language;
            for (let iterator = 0; iterator<parts.length; ++iterator) {
                const word = parts[iterator].toLowerCase();

                if (!word.includes("intern") && !word.includes("developer") && !word.includes("senior") && !word.includes("-")) {
                    language = word;
                    break;
                }
                
            }

            if (!languageGroups[language]) {
                languageGroups[language] = [];
            }

            if (language) {
                languageGroups[language].push(user);
            }
            
        }
        console.log(languageGroups);
        return languageGroups;
    } else {
        return console.log("Given data is not in correct format");
    }
}

module.exports = groupUsersProgramming;